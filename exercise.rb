class Exercise

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
    edited_strings = str.split(' ').map do |word|
      if word.length > 4
        if word[0] == word[0].upcase
          word.gsub(/\w+/,'Marklar')
        else
          word.gsub(/\w+/,'marklar')
        end
      else
        word
      end
    end
    edited_strings.join(' ')
  end

  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.even_fibonacci(nth)
    sum = 0
    counter = 1
    fib_array = [0,1]
    while counter < nth do
      fib_array << fib_array[-2] + fib_array[-1]
      counter += 1
    end
    fib_array.each do |number|
      if number % 2 == 0
        sum += number
      end
    end
    sum
  end

end
